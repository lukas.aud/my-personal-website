// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'Test website is live': browser => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL)
      .waitForElementVisible('#app', 2500)
      .end()
  },
  'Test navbar exists': browser => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL)
      .waitForElementVisible('#nav', 2500)
      .end()
  },
  'Test navbar redirects to other pages': browser => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL)
      .waitForElementVisible('#nav', 2500)
      .click('#nav-link-1')
      .assert.urlEquals('http://localhost:8080/Projects')
      .waitForElementVisible('#nav', 2000)
      .click('#nav-link-2')
      .assert.urlEquals('http://localhost:8080/Contact')
      .waitForElementVisible('#nav', 2000)
      .click('#nav-link-0')
      .assert.urlEquals('http://localhost:8080/')
      .waitForElementVisible('#nav', 2000)
      .end()
  },
  'Test projects page loads up': browser => {
    browser
    .url(process.env.VUE_DEV_SERVER_URL)
    .waitForElementVisible('#nav', 2500)
    .click('#nav-link-1')
    .assert.urlEquals('http://localhost:8080/Projects')
    .waitForElementVisible('#nav', 2000)
    .assert.containsText('h1', 'My projects')
    .end()
  },
  'Test contact page loads up': browser => {
    browser
    .url(process.env.VUE_DEV_SERVER_URL)
    .waitForElementVisible('#nav', 2500)
    .click('#nav-link-2')
    .assert.urlEquals('http://localhost:8080/Contact')
    .waitForElementVisible('#nav', 2000)
    .assert.containsText('h1', 'Do you want to contact me?')
    .end()
  },
  'Test local API works': browser => {
    browser
    .url(process.env.VUE_DEV_SERVER_URL)
    .waitForElementVisible('#app', 2000)
    .click("#nav-link-2")
    .assert.urlEquals('http://localhost:8080/Contact')
    .waitForElementVisible('#api', 2000)
    .assert.containsText('#api','laudzevicius24@hotmail.com')
    .assert.containsText('#api','Personal')
    .end()
  },
  'Test modals "detail" buttons display modals': browser => {
    browser
    .url(process.env.VUE_DEV_SERVER_URL)
    .waitForElementVisible('#app', 2000)
    .click('#nav-link-1')
    .assert.urlEquals('http://localhost:8080/Projects')
    .waitForElementVisible('#project1', 10000)
    .click('#project1-button')
    .waitForElementVisible('#project1-modal', 1000)
    .end()
  },
  'Test clicking hyperlinks in contacts page redirects to other pages': browser => {
    browser
    .url(process.env.VUE_DEV_SERVER_URL)
    .waitForElementVisible('#app', 2000)
    .click('#nav-link-2')
    .assert.urlEquals('http://localhost:8080/Contact')
    .waitForElementVisible('.txtbck', 2000)
    .click('#h-link1')
    .assert.urlEquals('https://e-mailer.link/en/')
    .end()
  },
  'Test form in contact page': browser => {
    browser
    .url(process.env.VUE_DEV_SERVER_URL)
    .waitForElementVisible('#app', 2000)
    .click('#nav-link-2')
    .assert.urlEquals('http://localhost:8080/Contact')
    .waitForElementVisible('#input1', 2000)
    .setValue('#input1', "alexCarter@outlook.com")
    .setValue('#input2', "Alexander Carter")
    .setValue('#input3', "Other")
    .click('#input-submit')
    .waitForElementVisible('#app', 2000)
    .assert.urlEquals('http://localhost:8080/Contact')
    .assert.containsText('#api', 'alexCarter@outlook.com')
    .assert.containsText('#api', 'Alexander Carter')
    .assert.containsText('#api', 'Other')
    .end()
  }
}
