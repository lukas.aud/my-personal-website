import { shallowMount } from '@vue/test-utils'
import Projects from '@/components/Projects.vue'

describe('Homepage.vue', () => {
  it('renders props.msg2 when passed', () => {
      const msg2 = 'Coming soon'
      const wrapper2 = shallowMount(Projects, {
          propsData: { msg2 }
      })
      expect(wrapper2.text()).toMatch(msg2)
  })

  it('renders props.msg3 when passed', () => {
    const bod = 'No information available.'
    const wrapper3 = shallowMount(Projects, {
        propsData: { bod }
    })
    expect(wrapper3.text()).toMatch(bod)
})
})